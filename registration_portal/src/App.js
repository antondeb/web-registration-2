import React, { Component } from 'react';
import './App.css';

import Backgroundimg from './components/backgroundimg'
import LandingPageCard from './components/landingpagecard'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header" >
          <Backgroundimg /> 
          <div className='App-Event-Info'>
            <h1 className="App-title">XL Day: About Now</h1>
            <p>6th December / Eindhoven, Klokgebouw 50</p>
          </div>
        </header>

        <body>
          <LandingPageCard />
        </body>
        
      </div>
    );
  }
}

export default App;
