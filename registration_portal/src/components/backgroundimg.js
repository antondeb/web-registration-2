import React from 'react';
import Blur from 'react-blur';
import background_image from '../images/backgroundimage.jpg';

function backgroundimg(props) {
    return (
    <Blur img={background_image} blurRadius={75} style={{ height: 300, width: '100%', position: 'absolute', zIndex: -1, opacity: 0.83 }}>
    </Blur>
    )
  }

export default backgroundimg