import React, { Component } from 'react';
import '../App.css';
import background_image from '../images/backgroundimage.jpg';

class LandingPageCard extends Component {
    constructor(props) {
        super();
    }

    render() {
      return (
        <div className='Landing-Page-Card'>
            <div style={{display: 'left', borderBottom: '1px solid rgb(230, 230, 230)'}}>
                <img id='cardimage' src= {background_image} alt='backgorundimage' style={{ verticalAlign: 'bottom' }}/>
                <span style={{float: 'right', margin: '10px', textAlign: 'left'}}>
                    <h4 className='descriptiontext'> Date & Time </h4>
                    <p className='descriptiontext'>6th December / Eindhoven, Klokgebouw 50</p>
                    <h4 className='descriptiontext' style={{marginTop: '20px'}}> Location </h4>
                    <p className='descriptiontext'>Klokgebouw</p>
                    <p className='descriptiontext'>50 Klokgebouw</p>
                    <p className='descriptiontext'>3465 CB Eindhoven</p>
                    <h2 className='descriptiontext' style={{marginTop: '20px'}}> XL Day: About Now </h2>

                    <div style={{position: 'relative', display: 'flex', justifyContent: 'center', bottom:'0px', margin: 'auto'}}>
                        <button id='registernowbtn'> Register Now </button>
                    </div>

                </span>
            </div>
            <h1> lalalala </h1>
        </div>
      );
    }
  }

  export default LandingPageCard